/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chonchanok.oxoop;

import java.util.Scanner;

/**
 *
 * @author Van
 */
public class Game {
 Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        //Normal flow
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX, playerO);
        System.out.println("Start a New Game");
    }
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish()){
                if(table.getWinner()==null){
 		    this.showTable();
                    System.out.println("Draw!!!");
                }else{
		    this.showTable();
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                newGame();
            }
            table.switchPlayer();
        }
    }
}
