/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chonchanok.oxoop;

/**
 *
 * @author Van
 */
public class Player {

    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        win++;
    }

    public void lose() {
        lose++;
    }

    public int getLose() {
        return lose;
    }

    public void draw() {
        draw++;
    }

    public int getDraw() {
        return draw;
    }

}
